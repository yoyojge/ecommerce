const products = [
    {
      id: '1',
      title: 'Lenovo IdeaPad 5',
      description:
        'PC Portable Lenovo IdeaPad 5 15ALC05 15,6 AMD Ryzen 7 8 Go RAM 512 Go SSD Gris graphite ',
      price: '599.99',
      category: 'laptop',
      note: '5',
      src:"../images/ideapad5.jpg"
    },
    {
      id: '2',
      title: 'Asus VivoBook',
      description:
        'PC portable Asus VivoBook S1500EA-EJ2986W 15.6" Intel Core i5 16 Go RAM 512 Go SSD Gris  ',
      price: '719.99',
      category: 'laptop',
      note: '4.9',
      src:"../images/ideapad5.jpg"
    },
    {
      id: '3',
      title: 'Dell Inspiron 15-3000',
      description:
        'PC Portable Dell Inspiron 15-3000 15.6" Intel Core i7 16 Go RAM 512 Go SSD Noir carbone  ',
      price: '609.99',
      category: 'laptop',
      note: '4.5',
      src:"../images/ideapad5.jpg"
    },
    {
      id: '4',
      title: 'Portable HP 15s',
      description:
        'PC Portable HP 15s-eq2026nf 15,6" AMD Ryzen 5 16 Go RAM 512 Go SSD Argent naturel  ',
      price: '699.99',
      category: 'laptop',
      note: '3.8',
      src:"../images/ideapad5.jpg"
    },
    {
      id: '5',
      title: 'Acer Aspire Vero',
      description:
        'PC Portable Acer Aspire Vero AV15-51R-550M 15,6" Intel Core i5 8 Go RAM 512 Go SSD Gris Edition National Geographic ',
      price: '699.99',
      category: 'laptop',
      note: '4.6',
      src:"../images/ideapad5.jpg"
    },
    {
      id: '6',
      title: 'Portable HP Omen 16',
      description:
        'PC Portable HP Omen 16-b0065nf 16,1" Intel Core i7 16 Go RAM 512 Go SSD Nvidia RTX 3050 Noir ombré ',
      price: '1349.99',
      category: 'laptop',
      note: '5',
      src:"../images/ideapad5.jpg"
    },
    {
      id: '7',
      title: 'Tout en un HP Pavilion',
      description:
        'PC Tout en un HP Pavilion 24-k0002nf 23,8" AMD Ryzen 5 8 Go RAM 512 Go SSD Blanc neige ',
      price: '869.99',
      category: 'desktop',
      note: '4.3',
      src:"../images/ideapad5.jpg"
    },
    {
      id: '8',
      title: 'Tout en un Acer Aspire C27',
      description:
        'PC Tout en un Acer Aspire C27-1655 27" Intel Core i5 8 Go RAM 256 Go SSD + 1 To HDD Blanc et noir',
      price: '959.99',
      category: 'desktop',
      note: '3.9',
      src:"../images/ideapad5.jpg"
    },
    {
      id: '9',
      title: 'Tout-en-un Dell Inspiron 7700 ',
      description:
        'PC Tout-en-un Dell Inspiron 7700 27" Intel Core i5 8 Go RAM 512 Go SSD Argent métallique',
      price: '958.99',
      category: 'desktop',
      note: '3.5',
      src:"../images/ideapad5.jpg"
    },
    {
      id: '10',
      title: 'Tout en un Lenovo IdeaCentre 3',
      description:
        'PC Tout en un Lenovo IdeaCentre 3 27ALC6 27" AMD Ryzen 5 8 Go RAM 512 Go SSD Noir',
      price: '999.99',
      category: 'desktop',
      note: '4.4',
      src:"../images/ideapad5.jpg"
    },
  ];
export default products