
import React from "react";
import {useState}  from 'react' ; 

import { Link } from "react-router-dom";

export default function Boutique(props) {

    // console.log(props);

    //le hook useState nous permet d'utiliser un state dans un composant fonctionnel
    const  [sorting,setSorting]   = useState({
        filterBy:"desktop"
    }) ;


    const handleClick = (e) =>{
        e.preventDefault();

        setSorting( (prevState) => {
            return {               
                filterBy : prevState.filterBy === "desktop" ? "laptop" : "desktop"               
            }
        } )

    }


    const filterProducts = (products) => {
       return products.filter( product => product.category === sorting.filterBy  )     
      }

     return (
         <div>
             <h1>Boutique 🧮</h1>


             <table style={{width: "100%"}}  className="table table-dark table-striped">
                <thead>
                    <tr>
                        <th>RANG</th>
                        <th style={{textAlign : "start",}}>MODELE</th>
                        <th style={{textAlign : "start",}}>DESCRIPTION</th>
                        <th>PRIX</th>
                        <th style={{textAlign : "center",cursor:"pointer"}}>                             
                            <a href onClick={handleClick}>CATEGORIE</a>
                        </th>
                        <th style={{textAlign : "center",}}>NOTE</th>
                        <th style={{textAlign : "center",}}>PHOTO</th>
                    </tr>
                </thead>
                <tbody> 
            
            {
             filterProducts(props.products).map(
                        (product) => {

                           
                                return (
                                    
                                    <tr key={product.id}>
                                        <th><Link to={ `/detail/${product.id}` }>{product.id}</Link></th>
                                        <th style={{textAlign : "start",}}>{product.title}</th>
                                        <th style={{textAlign : "start",}}>{product.description}</th>
                                        <th>{product.price}</th>
                                        <th style={{textAlign : "center",}}>{product.category}</th>
                                        <th style={{textAlign : "center",}}>{product.note}</th>
                                        <th style={{textAlign : "center",}}><img src={product.src} alt={product.title} height="100px" /></th>
                                    </tr>
                                )
                              

                        }
                    )

                }
                 

                
                 </tbody>
            </table>

         </div>
     )
 }