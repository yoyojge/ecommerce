
import React from "react";
import { useParams } from 'react-router-dom'

export default function Detail(props) {
     
     //recuperation de l'id par la passage de parametre dans l'url
     const { idProduct } = useParams();

     // console.log(props);

     let uniqueProduct = props.products.filter( product => product.id === idProduct  )  


     const handleClick = (e) =>{
          e.preventDefault() ;         
          props.addToPanier(idProduct)
     }


     return (
          <div>
              <h1>Detail 🧮</h1>

              <table style={{width: "100%"}}  className="table table-dark table-striped">
                <thead>
                    <tr>
                        <th>RANG</th>
                        <th style={{textAlign : "start",}}>MODELE</th>
                        <th style={{textAlign : "start",}}>DESCRIPTION</th>
                        <th>PRIX</th>
                        <th style={{textAlign : "center"}}>CATEGORIE</th>
                        <th style={{textAlign : "center",}}>NOTE</th>
                        <th style={{textAlign : "center",}}>PHOTO</th>
                    </tr>
                </thead>
                <tbody> 

               {
               uniqueProduct.map(
                        (product) => {                           
                              return (
                                    
                                    <tr key={product.id}>
                                        <th>{product.id}</th>
                                        <th style={{textAlign : "start",}}>{product.title}</th>
                                        <th style={{textAlign : "start",}}>{product.description}</th>
                                        <th>{product.price}</th>
                                        <th style={{textAlign : "center",}}>{product.category}</th>
                                        <th style={{textAlign : "center",}}>{product.note}</th>
                                        <th style={{textAlign : "center",}}><img src={product.src} alt={product.title} height="100px" /></th>
                                    </tr>
                              )   
                        }
                    )

                }
                 

                
                 </tbody>
            </table>

                <div style={{ display: "flex", flexDirection: "line", alignItems:"center", justifyContent:"center" }}>
                    <select>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                    </select>
                    <button type="button"  onClick={handleClick}>Ajouter au panier</button>
               </div>






          </div>
     );


}