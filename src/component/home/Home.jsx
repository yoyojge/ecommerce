import React from "react";

import Section from "./Section";

export default class Home extends React.Component {

    constructor(props) {
        super(props)
    }
    render() {

       
        return (
            
            <React.Fragment>
                 <h1>TOP 3 🧮</h1>
                <Section products={this.props.products}/>

            </React.Fragment>
        )
    }
} 