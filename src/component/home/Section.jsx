import React from "react";
import { Link } from "react-router-dom";

export default class Section extends React.Component {
     
    render() {
        // console.log(this.props.products);

        this.props.products.sort( 
            (a,b) => {
                if(a.note < b.note){
                    return 1;
                }else if(a.note > b.note){
                return  -1;
                }else {
                return 0;
                }
            }
            );
            let counter = 0;
    
        return (
            
            <table style={{width: "100%"}}  className="table table-dark table-striped">
                <thead>
                    <tr>
                        <th>RANG</th>
                        <th style={{textAlign : "start",}}>MODELE</th>
                        <th style={{textAlign : "start",}}>DESCRIPTION</th>
                        <th>PRIX</th>
                        <th style={{textAlign : "center",}}>CATEGORIE</th>
                        <th style={{textAlign : "center",}}>NOTE</th>
                        <th style={{textAlign : "center",}}>PHOTO</th>
                    </tr>
                </thead>
                <tbody> 
                    
                    {         
                            
                        this.props.products.map(
                            (product) => {

                                counter++;
                                if(counter <4){
                                    return (
                                        
                                        <tr key={product.id}>
                                            <th><Link to={ `/detail/${product.id}` }>{product.id}</Link></th>
                                            <th style={{textAlign : "start",}}>{product.title}</th>
                                            <th style={{textAlign : "start",}}>{product.description}</th>
                                            <th>{product.price}</th>
                                            <th style={{textAlign : "center",}}>{product.category}</th>
                                            <th style={{textAlign : "center",}}>{product.note}</th>
                                            <th style={{textAlign : "center",}}><img src={product.src} alt={product.title} height="100px" /></th>
                                        </tr>
                                    )
                                }   

                            }
                        )
                    
                    }                 

                
                </tbody>
            </table>
        )
    }
} 