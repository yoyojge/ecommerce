import React from "react";
import PC from '../PC.svg';
import Nav from "./Nav";

export default class Header extends React.Component {

    constructor() {
        super()
    }
    render() {
        return (
            <div>
                <header style={{ display: "flex", justifyContent: "space-between", alignItems: "center", margin: "20px" }}>

                    <img src={PC} alt="logo" width={100}  ></img>
                    <Nav />

                </header>

            </div>
        )
    }
}