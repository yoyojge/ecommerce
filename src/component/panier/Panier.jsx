

// pouvoir supprimer des lignes du panier
//faire un composant suppression poubelle dans le panier
//faire un composant ajout bouton ajout dans panier..



import React from "react";


export default function Panier(props) {
    let totalPrice = 0;

    //recuperation du localStorage pour affichage du contenu du panier
    const LSmonPanier = JSON.parse(localStorage.getItem('LSmonPanier'));


    const handleClickSupp = (e, itemId)=> {
        e.preventDefault();
        props.suppFromPanier(itemId);
    };    

     return (
          <div>
          <h1>Panier 🧮</h1>

          <table style={{width: "100%"}}  className="table table-dark table-striped">
                <thead>
                    <tr>      
                        <th>ID</th>                  
                        <th style={{textAlign : "start",}}>MODELE</th>
                        <th style={{textAlign : "start",}}>DESCRIPTION</th>
                        <th>PRIX</th>
                        <th>QTITE</th>
                        <th>&nbsp;</th>                       
                    </tr>
                </thead>
                <tbody> 

               {

                


                props.monPanier.length > 0 &&
                props.monPanier.map(
                // LSmonPanier.map(
                        (product) => {

                         totalPrice += Number(product.price);

                            return (                                   
                                <tr key={product.id}>                                   
                                    <th>{product.id}</th>
                                    <td style={{textAlign : "start",}}>{product.title}</td>
                                    <td style={{textAlign : "start",}}>{product.description}</td>
                                    <td>{product.price}</td>  
                                    <td style={{textAlign : "center",}}>1</td>    
                                    <td onClick={e => handleClickSupp(e, product.id)} style={{cursor:"pointer"}}>
                                        <img src={window.location.origin + '/images/trash.png'} alt="supp" width="20" />
                                    </td>                                 
                                </tr>
                            )  
                        }
                    )
                }       

                
                 </tbody>
                 <tfoot>
                    <tr>                        
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td style={{textAlign : "end"}}>TOTAL : </td>
                        <td colspan="3">{totalPrice.toFixed(2)} €</td>   
                    </tr>
                </tfoot>
            </table>
            </div>
     )

}