import React from "react";



import { Link } from "react-router-dom";

  


export default class Nav extends React.Component {

    constructor() {
        super()
    }
    render() {
        return (
            <React.Fragment>
            <nav style={{ display: "flex", flexDirection: "line" }}>
                <h3 style={{ margin: "10px" }}><Link to="/">Accueil</Link></h3>
                <h3 style={{ margin: "10px" }}><Link to="/boutique">Boutique</Link></h3>
                <h3 style={{ margin: "10px" }}><Link to="/panier">Panier</Link></h3>
               
            </nav>

           
            </React.Fragment>

        )
    }
}