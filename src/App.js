import './App.css';
import React from "react";
import Header from "./component/Header";
import products from "./Data";

//pour le rooting
import { Route, Routes } from "react-router-dom"
import Boutique from './component/boutique/Boutique';
import Home from './component/home/Home';
import Detail from './component/detail/Detail';
import Panier from './component/panier/Panier';

export  default class App extends React.Component{
    constructor() {
        super();
        this.state = {
            stateMonPanier:[],
            addToPanier:this.addToPanier,
            suppFromPanier:this.suppFromPanier
        }
        this.addToPanier = this.addToPanier.bind(this);
        this.suppFromPanier = this.suppFromPanier.bind(this);    
    }


    componentDidMount() {    
      //initalisation du localstorage si pas deja existant :: !!! LSmonPanierutilisé dans le composant panier
      if( localStorage.getItem('LSmonPanier') === null ){        
        console.log("localstorage vide");
        localStorage.setItem('LSmonPanier', JSON.stringify(this.state.stateMonPanier));         
      }
      else{        
        // this.state.stateMonPanier = [...JSON.parse( localStorage.getItem('LSmonPanier') )];
        // console.log("coucou");
        this.setState({stateMonPanier:[...JSON.parse( localStorage.getItem('LSmonPanier') )]})
      }
    }


    componentDidUpdate(){
      //je transforme en string mon state 
      let stateStringified = JSON.stringify(this.state.stateMonPanier) ;  
      //puis je peux l'enregistrer dans mon localstorage
      localStorage.setItem("LSmonPanier",stateStringified);
    }


    addToPanier(newItemId){
        let uniqueProduct = products.filter( product => product.id === newItemId  ) ;  
        console.log(newItemId);
        this.setState( (prevState) => {
          return {
            stateMonPanier: [...prevState.stateMonPanier, uniqueProduct.shift()]            
          }          
      });
    }

    
    suppFromPanier(newItemId){
      console.log(localStorage.getItem('LSmonPanier'));
      let copyState = JSON.parse( localStorage.getItem('LSmonPanier') );
      // console.log(copyState);
      let newState = copyState.filter( productPc => productPc.id !== newItemId )
      this.setState({stateMonPanier:newState})
    }


    render() {
      // console.log(products)
      return(
      <React.Fragment>
          <Header/>      

          <Routes>
            <Route exact component path="/" element={<Home products={products} />} />
            <Route path="/boutique" element={<Boutique products={products}/>} />
            <Route path="/panier" element={<Panier suppFromPanier={this.suppFromPanier} monPanier={this.state.stateMonPanier} />} />
            <Route path="/detail/:idProduct" element={<Detail products={products} addToPanier={this.addToPanier} />} />
          </Routes>
        
        
      </React.Fragment>
      )
    }
}
